{
  description = "Calliope's NixOS Configurations";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    home-manager.url = "github:nix-community/home-manager/release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    #agenix.url = "github:ryantm/agenix";
    #agenix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, home-manager }@inputs: rec {
    legacyPackages = nixpkgs.lib.genAttrs [ "x86_64-linux" ] (system:
      import inputs.nixpkgs {
        inherit system;

        # unfree packages
        config.allowUnfree = true;

        permittedInsecurePackages = [
          "vault-1.14.10"
        ];
      }
    );

    # Calliope\ for local development
    devShell.x86_64-linux = (legacyPackages.x86_64-linux.callPackage ./shell.nix {
      pkgs = legacyPackages.x86_64-linux;
    });

    # Calliope\ export this module too, so it can be consumed by other people.
    nixosModules = rec {
      gitlab = import ./modules/gitlab;
      default = gitlab;
    };

    # Calliope\ machine-specific derivations
    nixosConfigurations = {
      # Work laptop
      codeine = nixpkgs.lib.nixosSystem {
        pkgs = legacyPackages.x86_64-linux;
        system  = "x86_64-linux";
        modules = [
          # Global machine stuff
          ./hosts/codeine
          # ERD components
          ./modules/gitlab
          # Home manager configurations
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = false;
            home-manager.useUserPackages = true;
            home-manager.users.calliope = import ./home.nix;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
            home-manager.extraSpecialArgs = {
              withGUI = true;
            };
          }
          #agenix.nixosModules.default
          #{
          #  environment.systemPackages = [ agenix.packages.x86_64-linux.default ];
          #}
        ];
      };
    };
  };
}