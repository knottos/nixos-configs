{ 
  pkgs,
  email,
  serialNumber
}:
let
  # We make a small wrapper for s1 binaries, cos y'know
  # they need to run unmodified.
  s1wrapper = pkgs.buildFHSUserEnv {
    name = "s1wrapper";
    targetPkgs = pkgs: with pkgs; [
    ];
    runScript = "$SHELL";
  };
in
pkgs.stdenv.mkDerivation rec {
  pname   = "sentinelone";
  version = "24_1_2_6";

  # Calliope\ Fetch s1 installer from the IT security repo
  # You'll need to have set up your private git credentials and
  # have correct access for this to work.
  srcs = [
    #(builtins.fetchGit {
    #  url = "git@gitlab.com:gitlab-com/it/security/sentinelone-installers.git";
    #  rev = "f07264a9dcd6cdc0b9a1f865ba736421e72220e7";
    #  shallow = true;
    #})

    /home/calliope/src/gitlab.com/gitlab-com/it/security/sentinelone-installers/SentinelAgent_linux_x86_64_v${version}.deb
    /home/calliope/src/gitlab.com/gitlab-com/it/security/sentinelone-installers/config.cfg
    ./config
    ./bootstrap-sentinelone
  ];

  unpackPhase = ''
    for s in $srcs; do
      n=$(stripHash $s)
      cp -r $s $n
    done
    find .

    dpkg-deb -x ./SentinelAgent_linux_x86_64_v${version}.deb .
    #cp ./config.cfg .
  '';

  # Calliope\ Tell sentinelone that its actually installed itself
  # by cleverly writing all its configuration.
  buildPhase = ''
  export $(cat config.cfg | xargs)
  siteKey=$(echo $S1_AGENT_MANAGEMENT_TOKEN | base64 -d | jq .site_key)
  mgmtUrl=$(echo $S1_AGENT_MANAGEMENT_TOKEN | base64 -d | jq .url)

  sed -e '/^S1_AGENT_CUSTOMER_ID/d;' config.cfg > config.new
  mv config.new config.cfg

  sed -e 's/GITLAB_EMAIL_MACHINEID/${email}-${serialNumber}/g' config/policy.conf > policy.conf

  cat policy.conf
  echo "S1_AGENT_CUSTOMER_ID=${email}-${serialNumber}" >> config.cfg

    cat << EOF > basic.conf
{
    "mgmt_device-type": 1,
    "mgmt_site-key": $siteKey,
    "mgmt_url": $mgmtUrl
}
EOF
  '';
  dontPatchELF = true;
  noAuditTmpdir = true;
  nativeBuildInputs = [
    pkgs.dpkg
#    pkgs.autoPatchelfHook
    pkgs.zlib
    pkgs.stdenv.cc.cc.lib
    pkgs.libelf
    pkgs.dmidecode
    pkgs.jq
  ];

  # $out/opt represents the final state of the filesystem to be copied via bootstrap-sentinelone
  installPhase = ''
    mkdir -p $out/opt/
    mkdir -p $out/cfg/
    mkdir -p $out/bin/

    chmod +x bootstrap-sentinelone
    cp bootstrap-sentinelone $out/bin/

    cp -r opt/* $out/opt
    
    ln -s $out/opt/sentinelone/bin/sentinelctl $out/bin/sentinelctl
    ln -s $out/opt/sentinelone/bin/sentinelone-agent $out/bin/sentinelone-agent
    ln -s $out/opt/sentinelone/bin/sentinelone-watchdog $out/bin/sentinelone-watchdog

    ln -s $out/opt/sentinelone/lib $out/lib

    cp config/installation_params.json $out/cfg/
    cp config/local.conf $out/cfg/
    cp config/override.conf $out/cfg/

    cp basic.conf $out/cfg/
    cp policy.conf $out/cfg/
    cp config.cfg $out/cfg/install_config
  '';
}
