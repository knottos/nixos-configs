{ pkgs }:
let
in
# Calliope\ Build and patch drivestrike
pkgs.stdenv.mkDerivation rec {
  pname   = "drivestrike";
  version = "2.1.22-31";


  # https://app.drivestrike.com:443/static/apt/dists/stretch/Release
  # https://app.drivestrike.com:443/static/apt/dists/stretch/main/binary-amd64/Packages
  # deps = libsoup2.4-1 (>= 2.44.2), libglib2.0-0 (>= 2.40.0), util-linux (>=2.20.1), network-manager (>=0.9.8)
  src = pkgs.fetchurl {
    url = https://app.drivestrike.com:443/static/apt/pool/main/d/drivestrike/drivestrike_2.1.22-31_amd64.deb;
    sha256 = "sha256-blyXyt//0Pc28uHWZ4stsok9Se+y3JBL+lnSKnSCdgU=";
  };

  unpackPhase = ''
    dpkg-deb -x $src .
  '';

  buildPhase = ''
    cat << EOF > drivestrike-start
#!/usr/bin/env bash

set -x

if [ -e /etc/drivestrike.conf ]; then
  drivestrike run
else
  # This is dumb as it looks, but drivestrike assumes that nmcli is in /usr/bin/nmcli.
  mkdir -p /usr/bin

  ln -s ${pkgs.networkmanager}/bin/nmcli /usr/bin/nmcli 
  drivestrike register $GITLAB_EMAIL "" https://app.drivestrike.com:444/svc/
  drivestrike run
fi
EOF
  '';

  nativeBuildInputs = [
    pkgs.dpkg
    pkgs.dmidecode
    pkgs.autoPatchelfHook
    pkgs.libsoup
    pkgs.glib
  ];

  installPhase = ''
    mkdir -p $out/bin/
    chmod +x drivestrike-start
    cp drivestrike-start $out/bin/drivestrike-start
    cp -r usr/bin/drivestrike $out/bin/drivestrike
  '';
}
