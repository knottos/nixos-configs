{ config, lib, pkgs, ... }:
with lib;
let


in {
  options = {
    gitlab = {
      email = mkOption {
        type = types.str;
        example = "me@gitlab.com";
      };
      serialNumber = mkOption {
        type = types.str;
        example = "FTXYZWW";
      };
    };
  };

  imports = [
    ./pkgs/drivestrike
    ./pkgs/sentinelone
  ];

  # Calliope\ For ERD
  config.services.drivestrike.enable = true;
  config.services.sentinel-one.enable = true;
}