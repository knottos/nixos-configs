{ config, pkgs, ... }:

{
  services.displayManager = {
    defaultSession = "xfce";
    sddm = {
      enable = true;
    };
  };
  services.xserver = {
    enable = true;
    desktopManager = {
      xfce = {
        enable = true;
        noDesktop = true;
        enableXfwm = false;
      };
    };
  };
}