# common/default.nix

{ config, pkgs, ... }:

{
  imports = [
    ./users
  ];
 
  config = {
    # Mara\ Clean /tmp on boot.
    boot.tmp.cleanOnBoot = true;

    nix = {
      # Calliope\ Always enable nix-flakes
      package = pkgs.nixVersions.latest;
      extraOptions = ''
        experimental-features = nix-command flakes
        trusted-users = @wheel
      '';

      # Mara\ Automatically optimize the Nix store to save space
      # by hard-linking identical files together. These savings
      # add up.
      settings.auto-optimise-store = true;
    };

    # Calliope\ Always be in Europe/London
    time.timeZone = "Europe/London";

    # Calliope\ Never got this to work, so just disable it for now
    networking.useDHCP = false;

    # Calliope\ Select internationalisation properties.
    i18n.defaultLocale = "en_GB.UTF-8";
    console = {
      font = "Lat2-Terminus16";
      keyMap = "us";
    };
    
    # Mara\ Limit the systemd journal to 100 MB of disk or the
    # last 7 days of logs, whichever happens first.
    services.journald.extraConfig = ''
      SystemMaxUse=100M
      MaxFileSec=7day
    '';

    # Mara\ Use systemd-resolved for DNS lookups, but disable
    # its dnssec support because it is kinda broken in
    # surprising ways.
    services.resolved = {
      enable = true;
      dnssec = "false";
    };
  };
}
