{ config, lib, pkgs, inputs, ... }:
{ 
  config = {
    # Calliope\ Very important :)
    networking.hostName = "codeine";

    networking.nameservers = [
      "8.8.8.8"
      "8.8.4.4"
    ];
    hardware.pulseaudio.enable = false;

    # Calliope\ Use systemd rather than init
    # Sort out EFI 
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
    boot.loader.efi.efiSysMountPoint = "/boot/efi";

    boot.kernelModules = [ "kvm-amd" "kvm-intel" ];

    # Calliope\ Use a keyfile here
    boot.initrd.secrets = {
      "/crypto_keyfile.bin" = null;
    }; 

    # Calliope\ Set our location so redshift plays nice
    location.provider = "manual";
    location.latitude = 51.4545;
    location.longitude = 2.5879;

    # Calliope\ Use NetworkManager
    networking.networkmanager.enable = true;

    # Calliope\ For GitLab stuff.
    services.yubikey-agent.enable = true;

    # Calliope\ Some times I need this to test teleport locally
    networking.extraHosts =
      ''
        127.0.0.1  teleport.knottos.gitlab
        192.168.1.228 adrenaline
        127.0.0.1  unit_test.localhost
      '';

    # Calliope\ Some defaults that work well for me on my laptop.
    services.xserver.dpi = 180;
    services.xserver.xkb.layout = "gb";

    # Calliope\ Use nvidia drivers
    services.xserver.videoDrivers = ["nvidia"];
    
    # Calliope\ Bonus configuration that I also found works well.
    hardware.nvidia.open = false;
    hardware.nvidia.prime = {
      sync.enable = true;
      nvidiaBusId = "PCI:1:0:0";
      intelBusId = "PCI:0:2:0";
    };

    hardware.graphics.enable32Bit = true;

    # Calliope\ Enable the docker daemon
    virtualisation.docker.enable = true;
    hardware.nvidia-container-toolkit.enable = true;

    # Calliope\ Enable libvirtd
    virtualisation = {   
      libvirtd = {
        enable = true;
        qemu =  {
          package = pkgs.qemu_kvm;
          swtpm.enable = true;
          ovmf.enable = true;
          ovmf.packages = [ pkgs.OVMFFull.fd ];
        };
      };
      spiceUSBRedirection.enable = true;
    };
    # Calliope\ Enable passSecretService for org.freedesktop.secrets...
    services.passSecretService.enable = true;
    
    # Calliope\ For yubikey
    services.pcscd.enable = true;

    programs.ssh.knownHosts = {
      "gitlab.com" = {
        hostNames = [ "gitlab.com" ];
        publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf";
      };
    };

    programs.dconf.enable = true;
    programs.virt-manager.enable = true;
    programs.nix-ld.enable = true;
    programs.nix-ld.libraries = with pkgs; [
      # Add any missing dynamic libraries for unpackaged programs
      # here, NOT in environment.systemPackages
    ];

    gitlab.email = "cgardner@gitlab.com";
    gitlab.serialNumber = "F3TW5M3";

    system.stateVersion = builtins.trace "Evaluating system.stateVersion" "23.05";
  };
  

  imports = [
      # Calliope\ Include the hardware scan
      ./hardware-configuration.nix

      # Calliope\ Components we'd like to install
      # These are configured to my defaults, so they dont
      # take any arguments.
      ../../common
      ../../common/dm.nix

      ../../common/fonts.nix
      # ../../common/xterm
      
      # Calliope\ I share a keyboard between codeine and ibuprofen
      ../../common/hardware/miya68.nix
    ];
}
