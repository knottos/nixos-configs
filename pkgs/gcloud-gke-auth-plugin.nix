{ pkgs, vers, sha256, md5sum }:
let
in
pkgs.stdenv.mkDerivation rec {
  pname   = "gke-gcloud-auth-plugin";
  version = vers;

  # Calliope\ Fetch the gke-gcloud-auth-plugin
  # https://packages.cloud.google.com/apt/dists/cloud-sdk/main/binary-amd64/Packages

  src = pkgs.fetchurl {
    url = "https://packages.cloud.google.com/apt/pool/cloud-sdk/google-cloud-cli-gke-gcloud-auth-plugin_${version}_amd64_${md5sum}.deb";
    sha256 = sha256;
  };

  unpackPhase = ''
    dpkg-deb -x $src .
  '';

  nativeBuildInputs = [
    pkgs.dpkg
    pkgs.autoPatchelfHook
  ];

  installPhase = ''
    mkdir -p $out/bin
    cp usr/bin/gke-gcloud-auth-plugin $out/bin
  '';
}