# NixOS configurations

## Usage

```bash
# Be root.
sudo -i

# Save this for later.
cp /etc/nixos/hardware-configuration.nix ~/

# Remove all the existing configuration
rm -rf /etc/nixos/*

# Clone this repo, alternatively you could symlink this!
git clone git@gitlab.com:knottos/nixos-configs.git /etc/nixos/

# Put this back (should be gitignored)
mv ~/hardware-configuration.nix /etc/nixos/hardware-configuration.nix

# Rebuild system! (see requirements below for gotchas) 
nixos-rebuild --switch --impure --flake '.#codeine'
```


## Requirements

ComicCode font is required (see common/comiccode.nix); Disable this by just not importing the module, you'll also need to change common/xterm too.

Codeine's entry in flake.nix needs to be done in two parts, as we need to bootstrap the system known-hosts before we can pull private repos.

## Secrets

Example secrets file (referenced in hosts/codeine.nix)

```
{ config, pkgs, ... }:
{
  config.gitlab.email = "";
  config.gitlab.serialNumber = "";
}
```