{ config, lib, pkgs, specialArgs, ... }:
# Largely stolen from https://github.com/jonringer/nixpkgs-config
# Including all the polybar and i3 configuration, minorly adapted to my needs
# But they should get all the credit!

let
  packages = import ./packages.nix;

  inherit (specialArgs) withGUI;
  inherit (lib) mkIf;
in
{
  nixpkgs.config.allowUnfree = true;
  nixpkgs.overlays = [ ];

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "calliope";
  home.homeDirectory = "/home/calliope";
  home.stateVersion = "24.11";
  home.packages = packages pkgs withGUI;

  home.file.".config/polybar/pipewire.sh" = mkIf withGUI {
    source = pkgs.callPackage ./polybar.nix { };
    executable = true;
  };
  services.polybar = mkIf withGUI {
    enable = true;
    package = pkgs.polybarFull;
    config = pkgs.substituteAll {
      src = ./polybar-config;
      interface = "wlp147s0";
    };
    script = ''
      for m in $(polybar --list-monitors | ${pkgs.coreutils}/bin/cut -d":" -f1); do
        export MONITOR="$m"
        polybar nord &
      done
    '';
  };

  home.sessionVariables = {
    # Calliope\ Need this for glsh.
    USE_GKE_GCLOUD_AUTH_PLUGIN = "True";
    
    # Calliope\ I use kubectl to proxy vault
    # TODO: parameterize this.
    VAULT_ADDR = "https://localhost:8200";
    VAULT_TLS_SERVER_NAME = "vault.ops.gke.gitlab.net";

    GDK_SCALE = "2";
    GDK_DPI_SCALE = "0.75";
    _JAVA_OPTIONS = "-Dsun.java2d.uiScale=2";
    EDITOR = pkgs.lib.mkOverride 0 "vim";

    SSH_AUTH_SOCK = "/run/user/1000/yubikey-agent/yubikey-agent.sock";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  programs.alacritty = (import ./alacritty.nix) withGUI;
  programs.ssh = {
    enable = true;
    forwardAgent = true;
    extraConfig = ''
      Include ~/.ssh/config.d/*
      ##################################################
      # GitLab Hosts
      ##################################################

      #General settings; multiplexing for speeeed, keepalives to help azure connectivity
      Host *
        ControlMaster auto
        ControlPath ~/.ssh/master-%C
        ControlPersist 30m
        ServerAliveInterval 30

      Host *.c.gitlab-db-benchmarking.internal
        ProxyCommand ssh lb-bastion.db-benchmarking.gitlab.com -W %h:%p

      Host *.gitlab-ci-155816.internal
          ProxyJump   lb-bastion.ci.gitlab.com

      # Set the username and PreferredAuthentications here to avoid duplication
      Host *.gitlab-*.internal lb-bastion.*.gitlab.com
        # User calliope
        PreferredAuthentications publickey
        # Optional YubiKey cardno
        #IdentitiesOnly yes
        #IdentityFile             cardno:000000000000

      # HAProxy nodes use a different port
      Host haproxy-*.gitlab-*.internal
          Port 2222

      # Bastions for each GCP environment
      Host lb-bastion.*.gitlab.com
        StrictHostKeyChecking   no

      Host *.gitlab-*.internal
        ProxyCommand bash -c "ssh lb-bastion.`echo %h | cut -d\".\" -f1|awk -F- '{print \$NF}'`.gitlab.com -W %h:%p"

      # Console aliases
      Host gprd-console
        HostName     console-01-sv-gprd.c.gitlab-production.internal
        ProxyCommand ssh lb-bastion.gprd.gitlab.com -W %h:%p
      Host gstg-console
        HostName     console-01-sv-gstg.c.gitlab-staging-1.internal
        ProxyCommand ssh lb-bastion.gstg.gitlab.com -W %h:%p
      '';
  };
  programs.vscode = mkIf withGUI {
    enable = true;
    package = pkgs.vscode-fhsWithPackages (pkgs: with pkgs; [  ]);
    #extensions = with pkgs.vscode-extensions; [
    #  vscodevim.vim
    #  ms-python.python
    #];
  };
  programs.git = {
    enable = true;
    userName = "Calliope Gardner";
    userEmail = "cgardner@gitlab.com";

    extraConfig = {
      merge = {
        tool = "vimdiff";
        conflictstyle = "diff3";
      };
      pull = {
        rebase=true;
      };
      mergetool.prompt = "false";
      git.path = toString pkgs.git;
    };
  };
  services.gpg-agent = {
    enable = true;
    enableExtraSocket = withGUI;
    enableSshSupport = true;
    pinentryPackage = pkgs.pinentry-gtk2;
  };

  xsession = mkIf withGUI {
    enable = true;
    windowManager.i3 = rec {
      enable = true;
      package = pkgs.i3-gaps;
      config = {
        modifier = "Mod4";
        bars = [ ]; # use polybar instead

        gaps = {
          inner = 12;
          outer = 5;
          smartGaps = true;
          smartBorders = "off";
        };

        startup = [
          { command = "randr --output DP-0 --off --output DP-1 --off --output DP-2 --off --output DP-3 --off --output HDMI-0 --dpi 60 --primary --mode 3840x2160 --pos 0x0 --rotate right --output DP-4 --off --output DP-5 --dpi 60 --mode 3840x2160 --pos 2160x1680 --rotate normal --output eDP-1-1 --mode 1920x1080 --pos 6000x2760 --rotate normal --output DP-1-1 --off --output DP-1-2 --off"; notification = false;}
          { command = "feh --bg-scale ~/desktop-background.jpg"; }
          { command = "systemctl --user restart polybar"; always = true; notification = false; }
          { command = "systemctl --user restart yubikey-agent"; always = true; notification = false; }
        ];
        assigns = {
        };

        keybindings = import ./i3-keybindings.nix config.modifier;
      };
      extraConfig = ''
        for_window [class="^.*"] border pixel 2
        #exec systemctl --user import-environment
      '';
    };
  };
}