pkgs: withGUI: with pkgs; [
  # these files are meant to be installed in all scenarios
  vim
  git
  pinentry-gtk2
  wget
  pass
  dig
] ++ pkgs.lib.optionals withGUI [
  # intended to be installed with an X11 or wayland session
  firefox
  spotify
  scrot
  xclip
  dmenu
  feh

  rxvt-unicode-unwrapped
  sxiv
  vanilla-dmz
] ++ [
  # Gitlab-Related stuff
  google-cloud-sdk
  chef-cli
  doctl
  awscli2
  teleport
  kubectx
  kubectl
  _1password-cli

  (pkgs.callPackage ./pkgs/gcloud-gke-auth-plugin.nix {
    vers    = "504.0.0-0";
    md5sum  = "ac0995e251f5b8c8f61d6bac37c7ebfc";
    sha256  = "sha256-kQXZ/8RqOJYD92indNUj+MLFScR/lceg0JYUCXZQgrI=";
  })

  yubikey-personalization
  libu2f-host
  pcsctools
]